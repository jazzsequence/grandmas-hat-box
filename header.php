<!DOCTYPE html>
<?php tha_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php tha_head_top(); ?>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php wp_title() ?></title>
<link rel="Shortcut Icon" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/hatbox.ico" type="image/x-icon" />
<?php tha_head_bottom(); ?>
<?php wp_head(); ?>
<?php $options = get_option( 'ap_core_theme_options' ); ?>
</head>
<body <?php body_class(); ?>>
	<?php tha_body_top(); ?>
	<div class="container" id="wrap">
		<?php tha_header_before(); ?>
		<header>
			<?php tha_header_top(); ?>
			<div class="header">
				<?php wp_nav_menu( array( 'container' => 'nav', 'container_class' => 'topnav', 'theme_location' => 'top', 'fallback_cb' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new AP_Core_WP_Bootstrap_Navwalker() ) ); ?>
				<?php if ( (!get_header_image()) && (!has_post_thumbnail( $post->ID )) ) { ?>
				<hgroup class="siteinfo">
					<?php if ($options['alth1'] == 'true') { ?>
						<h1 class="alt"><a href="<?php echo home_url() ?>" title="<?php bloginfo('title'); ?>"><?php bloginfo('title'); ?></a></h1>
						<h2><?php bloginfo('description'); ?></h2>
					<?php } else { ?>
						<h1><a href="<?php echo home_url() ?>" title="<?php bloginfo('title'); ?>"><?php bloginfo('title'); ?></a></h1>
						<h2 class="alt"><?php bloginfo('description'); ?></h2>
					<?php } ?>
				</hgroup>
				<?php } else { ?>

			<div class="headerimg">
				<hgroup>
					<?php if ($options['alth1'] == 'true') { ?>
						<h1 id="sitetitle" class="alt"><a href="<?php echo home_url() ?>" title="<?php bloginfo('title'); ?>"><?php bloginfo('title'); ?></a></h1>
						<h2 id="tagline"><?php bloginfo('description'); ?></h2>
					<?php } else { ?>
						<h1 id="sitetitle"><a href="<?php echo home_url() ?>" title="<?php bloginfo('title'); ?>"><?php bloginfo('title'); ?></a></h1>
						<h2 id="tagline" class="alt"><?php bloginfo('description'); ?></h2>
					<?php } ?>
				</hgroup>
				<?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID );
					elseif ( get_header_image() ) : ?>
						<img src="<?php header_image(); ?>" width="<?php echo esc_attr( HEADER_IMAGE_WIDTH ); ?>" height="<?php echo esc_attr( HEADER_IMAGE_HEIGHT ); ?>" alt="" />
					<?php endif; ?>
			</div>
			<?php } ?>
			<div class="clear"></div>
				<?php wp_nav_menu( array( 'container' => 'nav', 'container_class' => 'mainnav', 'theme_location' => 'main', 'fallback_cb' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new AP_Core_WP_Bootstrap_Navwalker() ) ); ?>
			</div>
			<?php tha_header_bottom(); ?>
		</header>
		<?php tha_header_after(); ?>
		<div class="clear"></div>