# Grandma's Hat Box
Contributors: [jazzsequence](https://github.com/jazzsequence)
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=AWM2TG3D4HYQ6
Tags: two-columns, custom-menu, threaded-comments, sticky-post, variable-width, featured-image-header, featured-images, post-formats, right-sidebar
Requires at least: 3.1
Tested up to: 3.8.1
Stable tag: 1.1

A vintage-styled blog theme based on the Museum Core framework

## Description

Grandma’s Hat Box is inspired by vintage sewing and scrapbooking. It will give your site a unique, lovely look and remind you of cherished moments and treasured memories. Great for a photo blog about your little one’s cutest doings or an elegant journal filled with your own reflections.

## Installation

Grandma's Hat Box is a child theme of Museum Core.  To use Grandma's Hat Box, you must have Museum Core installed.

1.  Install Museum Core.  If you do not have a copy of Core, you can download it <a href="https://github.com/jazzsequence/AP-Museum_Core/zipball/master">here</a>.
2.  Unpack and upload the contents of `AP-Museum_Core.zip` to the `/wp-content/themes/` directory.
3. 	Install Grandma's Hat Box.
4. 	Unpack and upload the contents of `AP-Museum_Grandmas-Hat-Box.zip` to the `/wp-content/themes/` directory.
5.  Activate Grandma's Hat Box through the *Themes* menu in WordPress.
6.  That's it!

## Screenshots

![Grandma's Hat Box theme preview](https://assets1.assembla.com/code/grandmas-hat-box/git/node/blob/master/screenshot1.png)

Grandma's Hat Box theme preview

![Theme Options page](https://assets0.assembla.com/code/grandmas-hat-box/git/node/blob/master/screenshot2.png)

Theme Options page

![Custom Header page](https://assets2.assembla.com/code/grandmas-hat-box/git/node/blob/master/screenshot3.png)

Custom Header page

![Theme Update notification](https://assets1.assembla.com/code/grandmas-hat-box/git/node/blob/master/screenshot4.png)

Theme Update notification

## Changelog

### Version 1.1

* added more data sanitization
* changed the shadow on the title to make it more readable
* added a no-sidebar option
* added [Theme Hook Alliance](https://github.com/zamoose/themehookalliance) support
* streamlines the header
* added theme customizer support (removed theme options)
* replaced get_theme
* fixed some Bootstrap styles (for Museum Core 2.0 support)
* removed `load-options.php`

### Version 1.0.4

* fixed `cannot redeclare _checkactive_widgets`
* fixed issue preventing content from loading
* fixed undefined variable $ap_hatbox

### Version 1.0.3

* added 'museum-hatbox' and $ap_theme_dir globals to abstract the update-notification page
* removed readme.txt (there's really no need for another readme if there's already readme.md and readme.html)
* updated default footer (made it easier for translations)
* updated readme.html (corrected errors)

### Version 1.0.2

* removed undo core fonts (deprecated with Museum Core 1.0.9)
* fixed i18n stuff
* updates styles and templates for new grid system
* made download link dynamic in update notifier
* updated presstrends tracking code
* added 3.4 check for options page sidebar
* added reset for comments alt class

### Version 1.0.1

* removes redundant core scripts and fonts that aren't being used
* fixed generator
* removed redundant functions that can be loaded from Core
* added footer and fixed footer options (so they work)
* reverted to regular wp_title (custom title tags are handled in a wp_title filter in Museum Core)
* added readme.html
* added favicon

### Version 1.0

* added update notifier
* added nav menu styles
* defined a depth for dropdown menus (corrects weird styling issue if you have a 3-level dropdown)
* added theme options files & theme options page
* built child theme styles
* added images
* added header.php and functions.php
* created child theme style.css
* added readme files
* added GPLv3 license info