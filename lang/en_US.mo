��    '      T  5   �      `     a     p     �  P   �     �     �  /   �  	     7   "  �  Z  �        �     �     �     �       {       �	     �	     �	  
   �	     �	     �	  "   �	     
     
     !
     /
     @
     H
     _
  )   m
     �
     �
     �
  8   �
  $     	   1    ;     R     a     s  P   �     �     �  /   �  	   	  7     �  K  �   �     �     �     �     �     �  {       �     �     �  
   �     �     �  "   �     �                     1     9     P  )   ^     �     �     �  8   �  $   �  	   "                                          '                      $                           %   
       &   	                     "          #                   !                       %1$s %2$s %3$s %1$s Theme Update %1$s at %2$s %1$sGrandma's Hat Box%2$s is a %3$sWordPress%2$s theme by %4$sMuseum Themes%2$s. (Edit) (Rather than making changes to your theme, you might consider creating a child theme for your modifications.  %1$sHere's some information on building child themes%2$s.  You can also check out %3$smy presentation on child themes from WordCamp SLC 2011%2$s.) <cite>%s</cite> <span class="says">said:</span> Changelog Click here to download the latest version of your theme Deactivate the theme by switching to a different theme on the Appearance &rarr; Themes page.  Delete the old version of the theme by clicking on the Delete link from the Themes page.  (<strong>Note:</strong>  This <em>will</em> delete your theme, so this is not recommended if you have made modifications to the theme.) Click the Install Themes tab on the Themes page, then click the Upload link to upload the entire zip file. Extract the contents of the zip file to your computer and upload the entire folder via FTP to your <tt>/wp-content/themes/</tt> directory.  You're done!  Your theme has been updated! Footer Navigation Footer Text Full posts or excerpts? Grandma's Hat Box Grandma's Hat Box Options If you have made any changes to your theme, you will want to make a <strong>backup</strong> of the theme inside your WordPress themes folder ( <tt>/wp-content/themes/</tt> ).  The easiest way to do this is to rename the directory to something else (for example, <tt>%s-backup</tt>), or download the old theme folder to your computer before uploading the new version of the theme. Installation Left Sidebar Main Navigation No Sidebar On  Option 1: FTP Option 2: WordPress theme uploader Please note: Respond to this Right Sidebar Send usage data? Sidebar Theme Update Available Theme Updates There is a new version of %1$s available. Top Header Navigation Update Available Upgrade Instructions You have version %1$s installed. Update to version %2$s. Your comment is awaiting moderation. read more Project-Id-Version: Grandma's Hat Box
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-04 15:14-0700
PO-Revision-Date: 2014-04-04 15:15-0700
Last-Translator: Chris Reynolds <chris@jazzsequence.com>
Language-Team: Arcane Palette Creative Design <chris@arcanepalette.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.6.4
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 %1$s %2$s %3$s %1$s Theme Update %1$s at %2$s %1$sGrandma's Hat Box%2$s is a %3$sWordPress%2$s theme by %4$sMuseum Themes%2$s. (Edit) (Rather than making changes to your theme, you might consider creating a child theme for your modifications.  %1$sHere's some information on building child themes%2$s.  You can also check out %3$smy presentation on child themes from WordCamp SLC 2011%2$s.) <cite>%s</cite> <span class="says">said:</span> Changelog Click here to download the latest version of your theme Deactivate the theme by switching to a different theme on the Appearance &rarr; Themes page.  Delete the old version of the theme by clicking on the Delete link from the Themes page.  (<strong>Note:</strong>  This <em>will</em> delete your theme, so this is not recommended if you have made modifications to the theme.) Click the Install Themes tab on the Themes page, then click the Upload link to upload the entire zip file. Extract the contents of the zip file to your computer and upload the entire folder via FTP to your <tt>/wp-content/themes/</tt> directory.  You're done!  Your theme has been updated! Footer Navigation Footer Text Full posts or excerpts? Grandma's Hat Box Grandma's Hat Box Options If you have made any changes to your theme, you will want to make a <strong>backup</strong> of the theme inside your WordPress themes folder ( <tt>/wp-content/themes/</tt> ).  The easiest way to do this is to rename the directory to something else (for example, <tt>%s-backup</tt>), or download the old theme folder to your computer before uploading the new version of the theme. Installation Left Sidebar Main Navigation No Sidebar On  Option 1: FTP Option 2: WordPress theme uploader Please note: Respond to this Right Sidebar Send usage data? Sidebar Theme Update Available Theme Updates There is a new version of %1$s available. Top Header Navigation Update Available Upgrade Instructions You have version %1$s installed. Update to version %2$s. Your comment is awaiting moderation. read more 