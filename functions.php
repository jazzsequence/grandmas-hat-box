<?php
/* declare some global variables that we're going to use all over the place */
$ap_theme_dir = basename(dirname(__FILE__));

if ( !isset( $themecolors ) )
  $themecolors = array( 'bg' => 'f5eede', 'border' => 'f5eede', 'text' => '333333' );

/**
 * undo Core
 * @since 0.1
 * @author Chris Reynolds
 * @link http://justintadlock.com/archives/2010/12/30/wordpress-theme-function-files
 * removes the core setup action so we can run our own theme setup actions
 */

function ap_undo_core() {
    // remove ap_core_setup to prep for ap_hatbox_setup
    remove_action('after_setup_theme', 'ap_core_setup');
    // remove ap_core_custom_styles
    remove_action('wp_head','ap_core_custom_styles');
    // removes Core generator
    remove_action('wp_head','ap_core_generator');
}
add_action('after_setup_theme', 'ap_undo_core',9);

/**
 * undo core fonts
 * @author Chris Reynolds
 * @since 1.0
 * back again, unenqueues the fonts loaded in Core -- needed to unload the default fonts, if nothing else
 */

function ap_undo_corefonts() {
    wp_dequeue_style( 'droidsans' );
    wp_dequeue_style( 'ptserif' );
    wp_dequeue_style( 'inconsolata' );
    wp_dequeue_style( 'ubuntu' );
    wp_dequeue_style( 'lato' );
}
add_action('wp_enqueue_scripts', 'ap_undo_corefonts');

function ap_hatbox_no_sidebar() {
  $options = get_option( 'ap_hatbox_theme_options' );
  if ( 'none' == $options['sidebar'] )
    wp_enqueue_style( 'no-sidebar', get_stylesheet_directory_uri() . '/no-sidebar.css', '', '1.0' );
}
add_action( 'wp_enqueue_scripts', 'ap_hatbox_no_sidebar' );

/**
 * setup Hatbox
 * @uses add_theme_support()
 * @uses register_nav_menus()
 * @uses set_post_thumbnail_size()
 * @uses add_custom_background()
 * @since 0.2
 * adds core WordPress theme functionality and adds some tweaks
 */
function ap_hatbox_setup() {

    define( 'AP_HATBOX_DIR', get_stylesheet_directory_uri() );
    define( 'AP_HATBOX_IMAGES', AP_HATBOX_DIR . '/images/' );

    // load up the theme options
    require_once ( get_stylesheet_directory() . '/inc/theme-options.php' );
    require_once( get_template_directory() . '/inc/hooks.php' );
    require_once( get_template_directory() . '/inc/class-bootstrap-nav-walker.php' );

    /**
     * Update Notifier
     * @since 1.0
     * @author João Araújo
     * @link http://wplift.com/notify-your-theme-users-about-updates-in-their-dashboard/
     * this adds a (new) update notice that appears under the WordPress upgrades link in the dashboard and contains an admin page with installation instructions
     */
    include ( get_stylesheet_directory() . '/inc/update-notifier.php');

    // i18n stuff
    load_theme_textdomain('museum-hatbox', get_stylesheet_directory() .'/lang');

	// post thumbnail support
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 ); // 150 pixels wide by 150 pixels tall, box resize mode
    if ( ! isset( $content_width ) ) $content_width = 800;

    // custom nav menus
    // This theme uses wp_nav_menu() in three (count them, three!) locations.
    register_nav_menus( array(
    	'top' => __( 'Top Header Navigation', 'museum-hatbox' ),
    	'main' => __( 'Main Navigation', 'museum-hatbox' ),
    	'footer' => __( 'Footer Navigation', 'museum-hatbox' ),
    ) );


    // this theme has a custom header thingie
    // Your changeable header business starts here
    define( 'HEADER_TEXTCOLOR', '' );
    // No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
    define( 'HEADER_IMAGE', AP_HATBOX_IMAGES . 'headers/hat_box_header.png' );

    // The height and width of your custom header. You can hook into the theme's own filters to change these values.
    define( 'HEADER_IMAGE_WIDTH', apply_filters( 'core_header_image_width', 667 ) );
    define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'core_header_image_height', 266 ) );

    // We'll be using post thumbnails for custom header images on posts and pages.
    // We want them to be 940 pixels wide by 198 pixels tall.
    // Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
    set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

    // Don't support text inside the header image.
    define( 'NO_HEADER_TEXT', true );

    register_default_headers( array(
        'hatbox' => array(
        'url' => AP_HATBOX_IMAGES . 'headers/hat_box_header.png',
        'thumbnail_url' => AP_HATBOX_IMAGES . 'headers/hat_box_header-thumb.jpg',
        /* translators: header image description */
        'description' => __( 'Grandma\'s Hat Box', 'museum-hatbox' )
        )
    ) );

    // Add a way for the custom header to be styled in the admin panel that controls
    add_theme_support( 'custom-header' );
    // ... and thus ends the changeable header business.

    if ( ! function_exists( 'hatbox_admin_header_style' ) ) :
    /**
     * Styles the header image displayed on the Appearance > Header admin panel.
     *
     * Referenced via add_custom_image_header()
     *
     * @since 0.1
     */
    function hatbox_admin_header_style() {
    ?>
        <style type="text/css">
        /* Shows the same border as on front end */
        #headimg {
            border-bottom: 1px solid #000;
            border-top: 4px solid #000;
        }
        /* If NO_HEADER_TEXT is false, you would style the text with these selectors:
            #headimg #name { }
            #headimg #desc { }
        */
        </style>
    <?php
    }
    endif;

    // post formats
    // register all post formats -- child themes can remove some post formats as they so desire
    add_theme_support('post-formats',array('aside','gallery','link','image','quote','status','video','audio','chat'));

    // automatic feed links
    add_theme_support('automatic-feed-links');

    // this changes the output of the comments
    function ap_core_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>" class="the_comment">
      <div class="comment-author vcard">
         <?php echo get_avatar
    ($comment,$size='64',$default='<path_to_url>' ); ?>
    <?php _e('On ','museum-hatbox'); printf(__('%1$s at %2$s','museum-hatbox'), get_comment_date(), get_comment_time()) ?>
     <?php printf(__('<cite>%s</cite> <span class="says">said:</span>', 'museum-hatbox'), get_comment_author_link()) ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.', 'museum-hatbox') ?></em>
         <br />
      <?php endif; ?>
      <?php comment_text() ?>
      <div class="comment-meta commentmetadata"><?php edit_comment_link(__('(Edit)', 'museum-hatbox'),'  ','') ?></div>
      <div class="reply"><button>
         <?php comment_reply_link(array_merge
         ( $args, array('depth' => $depth, 'reply_text' => __('Respond to this','museum-hatbox'), 'max_depth' => $args['max_depth']))) ?>
      </button></div>
     </div>
    <?php
        }

    // this changes the default [...] to be a read more hyperlink
    function new_excerpt_more($more) {
        global $post;
        return '...&nbsp;(<a href="'. get_permalink($post->ID) . '">' . __('read more','museum-hatbox') . '</a>)';
    }
    add_filter('excerpt_more', 'new_excerpt_more');

    // loads theme version in the head
    add_action( 'tha_head_bottom', 'ap_core_generator' );

}
add_action('after_setup_theme','ap_hatbox_setup',11);

/**
 * Get default options
 * @since 0.4.0
 * @author Chris Reynolds
 * defines the options and some defaults
 */
function ap_hatbox_get_theme_defaults(){
    $footer_a = '<a href="http://museumthemes.com/museum-themes/grandmas-hat-box/" target="_blank">';
    $footer_b = '</a>';
    $footer_c = '<a href="http://wordpress.org" target="_blank">';
    $footer_d = '<a href="http://museumthemes.com/" target="_blank" title="Museum Themes">';
    // default options settings
    $defaults = array(
        // sidebar
    	'sidebar' => 'left',
        // theme tracking
      'presstrends' => true,
        // excerpts or full posts
      'excerpts' => true,
      // footer text
      'footer' => sprintf( __( '%1$s %2$s %3$s', 'museum-hatbox' ), '&copy;',  date('Y'), get_bloginfo('title') ) . ' . ' . sprintf( __( '%1$sGrandma\'s Hat Box%2$s is a %3$sWordPress%2$s theme by %4$sMuseum Themes%2$s.', 'museum-hatbox' ), $footer_a, $footer_b, $footer_c, $footer_d )
    );
    return $defaults;
}

/**
 * Sidebar settings
 * @since 0.4.0
 * @author Chris Reynolds
 * this is the array for the sidebar setting
 */
function ap_core_sidebar() {
  $sidebar = array(
    'left' => __( 'Left Sidebar', 'museum-core' ),
    'right' => __( 'Right Sidebar', 'museum-core' ),
    'none' => __( 'No Sidebar', 'museum-hatbox' )
  );
  return $sidebar;
}

/**
 * Get Which Sidebar
 * @since 2.0.1
 * @author Chris Reynolds
 * returns the sidebar classes
 */
function ap_core_get_which_sidebar() {
    $defaults = ap_hatbox_get_theme_defaults();
    $options = get_option( 'ap_hatbox_theme_options' );
    if ( isset( $options['sidebar'] ) ) {
        $sidebar = $options['sidebar'];
        if ( 'right' == $sidebar ) {
            $sidebar .= ' last';
        }
    } else {
        $sidebar = $defaults['sidebar'];
    }

    return $sidebar;
}

/**
 * Blog excerpt or full post
 * @since 2.0.1
 * @author Chris Reynolds
 * returns the blog excerpt option
 */
function ap_core_blog_excerpts() {
    $options = get_option( 'ap_hatbox_theme_options' );
    $defaults = ap_hatbox_get_theme_defaults();
    if ( !isset( $options['excerpts'] ) ) {
        $excerpt = $defaults['excerpts'];
    } else {
        $excerpt = $options['excerpts'];
    }

    return $excerpt;
}

/**
 * Archive excerpt or full post
 * @since 2.0.1
 * @author Chris Reynolds
 * returns the archive excerpt option
 */
  function ap_core_archive_excerpts() {
      $options = get_option( 'ap_hatbox_theme_options' );
      $defaults = ap_hatbox_get_theme_defaults();
      if ( !isset( $options['archive-excerpt'] ) ) {
          $excerpt = $defaults['archive-excerpt'];
      } else {
          $excerpt = $options['archive-excerpt'];
      }

      return $excerpt;
  }

if ( function_exists( '_checkactive_widgets' ) ) {
  // do nothing
}