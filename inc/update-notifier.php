<?php
/**
  * Provides a notification everytime the theme is updated
  * Original code courtesy of João Araújo of Unisphere Design - http://themeforest.net/user/unisphere
  */

/* had to fix this code quite a bit -- was really broken on the source post http://wplift.com/notify-your-theme-users-about-updates-in-their-dashboard */
function update_notifier_menu() {
	global $ap_theme_dir;
	$xml = get_latest_theme_version(21600); // This tells the function to cache the remote call for 21600 seconds (6 hours)
	$theme  = wp_get_theme( get_stylesheet_uri() );
	$theme_name = $theme['Name'];

	if(version_compare($theme['Version'], $xml->latest) == -1) {
		add_dashboard_page( $theme_name . __('Theme Updates','museum-hatbox'), __('Theme Update Available','museum-hatbox') . '<span class="update-plugins count-1" title="' . __( 'Update Available','museum-hatbox' ) . '"><span class="update-count">+' . $xml->latest . '</span></span>', 'administrator', $ap_theme_dir . '-updates', 'update_notifier');
	}
}

add_action('admin_menu', 'update_notifier_menu');

function update_notifier() {
	global $ap_theme_dir;
	$xml = get_latest_theme_version(21600); // This tells the function to cache the remote call for 21600 seconds (6 hours)
	$theme  = get_theme( get_current_theme() );
	$theme_name = get_current_theme(); ?>

	<style>
		.update-nag {display: none;}
		#instructions {max-width: 800px;}
		h3.title {margin: 30px 0 0 0; padding: 30px 0 0 0; border-top: 1px solid #ddd;}
		#message.updated { padding: 5px 0.6em; }
		.clear { clear: left; }
	</style>

	<div class="wrap">

		<div id="icon-tools" class="icon32"></div>
		<h2><?php printf(__('%1$s Theme Update','museum-hatbox'), $theme_name); ?></h2>
		<div id="message" class="updated"><strong><?php printf(__('There is a new version of %1$s available.','museum-hatbox'), $theme_name); ?></strong> <?php printf(__('You have version %1$s installed. Update to version %2$s.','museum-hatbox'), $theme['Version'], $xml->latest); ?></div>

 		<img style="float: left; margin: 0 20px 20px 0; border: 1px solid #ddd;" src="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/screenshot.png'; ?>" alt="" width="225" />
		<div id="instructions" style="max-width: 800px;">
		<h3><?php _e('Upgrade Instructions','museum-hatbox'); ?></h3>
		<strong><?php _e('Please note:','museum-hatbox'); ?></strong> <?php echo sprintf(__('If you have made any changes to your theme, you will want to make a <strong>backup</strong> of the theme inside your WordPress themes folder ( <tt>/wp-content/themes/</tt> ).  The easiest way to do this is to rename the directory to something else (for example, <tt>%s-backup</tt>), or download the old theme folder to your computer before uploading the new version of the theme.','museum-hatbox'), $ap_theme_dir ); ?>

		<p><?php echo sprintf(__('(Rather than making changes to your theme, you might consider creating a child theme for your modifications.  %1$sHere\'s some information on building child themes%2$s.  You can also check out %3$smy presentation on child themes from WordCamp SLC 2011%2$s.)','museum-hatbox'), '<a href="http://museumthemes.com/wiki/create-child-theme/" target="_blank">','</a>','<a href="http://wp.me/pllYY-27J" target="_blank">'); ?>

		<h4 class="clear"><?php _e('Installation','museum-hatbox'); ?></h4>

		<p><a href="http://downloads.museumthemes.com/<?php echo $ap_theme_dir; ?>.<?php echo $xml->latest; ?>.zip" target="_blank"><?php _e('Click here to download the latest version of your theme','museum-hatbox'); ?></a>.</p>

		<p><strong><?php _e('Option 1: FTP','museum-hatbox'); ?></strong></p>

		<p><?php _e('Extract the contents of the zip file to your computer and upload the entire folder via FTP to your <tt>/wp-content/themes/</tt> directory.  You\'re done!  Your theme has been updated!','museum-hatbox'); ?></p>

		<p><strong><?php _e('Option 2: WordPress theme uploader','museum-hatbox'); ?></strong></p>

		<p><?php _e('Deactivate the theme by switching to a different theme on the Appearance &rarr; Themes page.  Delete the old version of the theme by clicking on the Delete link from the Themes page.  (<strong>Note:</strong>  This <em>will</em> delete your theme, so this is not recommended if you have made modifications to the theme.) Click the Install Themes tab on the Themes page, then click the Upload link to upload the entire zip file.','museum-hatbox'); ?></p></div>
        <div class="clear"></div>

	    <h3 class="title"><?php _e('Changelog','museum-hatbox'); ?></h3>
	    <?php echo $xml->changelog; ?>
	</div>
	<?php }
	// This function retrieves a remote xml file on my server to see if there's a new update
	// For performance reasons this function caches the xml content in the database for XX seconds ($interval variable) function get_latest_theme_version($interval)
	function get_latest_theme_version($interval) {
		global $ap_theme_dir;
		// remote xml file location
	 	$notifier_file_url = 'http://downloads.museumthemes.com/' . $ap_theme_dir . '/notifier.xml';

	 	$db_cache_field = 'ap-notifier-cache';
	 	$db_cache_field_last_updated = 'ap-notifier-last-updated';
	$last = get_option( $db_cache_field_last_updated );
	$now = time();
	// check the cache
	if ( !$last || (( $now - $last ) > $interval) ) {
		// cache doesn't exist, or is old, so refresh it
		if( function_exists('curl_init') ) { // if cURL is available, use it...
			$ch = curl_init($notifier_file_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			$cache = curl_exec($ch);
			curl_close($ch);
		} else {
			$cache = file_get_contents($notifier_file_url); // ...if not, use the common file_get_contents()
		}

		if ($cache) {
			// we got good results
			update_option( $db_cache_field, $cache );
			update_option( $db_cache_field_last_updated, time() );
		}
		// read from the cache file
		$notifier_data = get_option( $db_cache_field );
	}
	else {
		// cache file is fresh enough, so read from it
		$notifier_data = get_option( $db_cache_field );
	}

	$xml = simplexml_load_string($notifier_data);

	return $xml;
}
?>