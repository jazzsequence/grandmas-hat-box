<?php
/**
 * Register theme settings
 * @since 0.1
 * @author Chris Reynolds
 * registers the settings
 */
function ap_hatbox_theme_options_init() {
    register_setting( 'AP_HATBOX_OPTIONS', 'ap_hatbox_theme_options', 'ap_hatbox_theme_options_validate' );
}
add_action ( 'admin_init', 'ap_hatbox_theme_options_init' );

/**
 * Use the theme customizer
 * @since 2.0.0
 * @author Chris Reynolds
 * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
 * uses the customizer for all the settings
 */
function ap_hatbox_theme_customizer( $wp_customize ) {
	$defaults = ap_hatbox_get_theme_defaults();

	class AP_Core_Textarea_Control extends WP_Customize_Control {

		public $type = 'textarea';

		public function render_content() {
			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<textarea rows="10" style="width:100%; font-family: monospace;" <?php $this->link(); ?>><?php echo esc_textarea( stripslashes_deep( $this->value() ) ); ?></textarea>
			</label>
			<?php
		}

	}

	/* add section */
	$wp_customize->add_section( 'ap_hatbox', array(
		'title' => __( 'Grandma\'s Hat Box Options', 'museum-hatbox' ),
		'priority' => 120
	) );

	/* add settings */
	$wp_customize->add_setting( 'ap_hatbox_theme_options[sidebar]', array(
		'default' => $defaults['sidebar'],
		'type' => 'option'
	) );

	$wp_customize->add_setting( 'ap_hatbox_theme_options[excerpts]', array(
		'default' => $defaults['excerpts'],
		'type' => 'option'
	) );

	$wp_customize->add_setting( 'ap_hatbox_theme_options[footer]', array(
		'default' => $defaults['footer'],
		'type' => 'option'
	) );

	$wp_customize->add_setting( 'ap_hatbox_theme_options[presstrends]', array(
		'default' => $defaults['presstrends'],
		'type' => 'option'
	) );

	/* add controls */
	$wp_customize->add_control( 'ap_hatbox_theme_options[sidebar]', array(
		'label' => __( 'Sidebar', 'museum-core' ),
		'section' => 'ap_hatbox',
		'settings' => 'ap_hatbox_theme_options[sidebar]',
		'type' => 'select',
		'choices' => ap_core_sidebar()
	) );

	$wp_customize->add_control( 'ap_hatbox_theme_options[excerpts]', array(
		'label' => __( 'Full posts or excerpts?', 'museum-hatbox' ),
		'section' => 'ap_hatbox',
		'settings' => 'ap_hatbox_theme_options[excerpts]',
		'type' => 'select',
		'choices' => ap_core_show_excerpts()
	) );

	$footer_text = new AP_Core_Textarea_Control( $wp_customize, 'ap_hatbox_theme_options[footer]', array(
		'label' => __( 'Footer Text', 'museum-core' ),
		'section' => 'ap_hatbox',
		'settings' => 'ap_hatbox_theme_options[footer]',
		'type' => 'textarea',
		'sanitize_callback' => 'esc_textarea'
	) );
	$wp_customize->add_control( $footer_text );

	$wp_customize->add_control( 'ap_hatbox_theme_options[presstrends]', array(
		'label' => __( 'Send usage data?', 'museum-core' ),
		'section' => 'ap_hatbox',
		'settings' => 'ap_hatbox_theme_options[presstrends]',
		'type' => 'select',
		'choices' => ap_core_true_false()
	) );

}
add_action( 'customize_register', 'ap_hatbox_theme_customizer' );

// Presstrends
function hatbox_presstrends() {

// PressTrends API Key
$api_key = 'i93727o4eba1lujhti5bjgiwfmln5xm5o0iv';

// Start of Metrics
global $wpdb;
$data = get_transient( 'presstrends_data' );
if (!$data || $data == ''){
$api_base = 'http://api.presstrends.io/index.php/api/sites/update/api/';
$url = $api_base . $api_key . '/';
$data = array();
$count_posts = wp_count_posts();
$count_pages = wp_count_posts('page');
$comments_count = wp_count_comments();
$theme_data = get_theme_data(get_stylesheet_directory() . '/style.css');
$plugin_count = count(get_option('active_plugins'));
$all_plugins = get_plugins();
$plugin_name = '';
foreach($all_plugins as $plugin_file => $plugin_data) {
$plugin_name .= $plugin_data['Name'];
$plugin_name .= '&';}
$posts_with_comments = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}posts WHERE post_type='post' AND comment_count > 0");
$comments_to_posts = number_format(($posts_with_comments / $count_posts->publish) * 100, 0, '.', '');
$pingback_result = $wpdb->get_var('SELECT COUNT(comment_ID) FROM '.$wpdb->comments.' WHERE comment_type = "pingback"');
$data['url'] = stripslashes(str_replace(array('http://', '/', ':' ), '', site_url()));
$data['posts'] = $count_posts->publish;
$data['pages'] = $count_pages->publish;
$data['comments'] = $comments_count->total_comments;
$data['approved'] = $comments_count->approved;
$data['spam'] = $comments_count->spam;
$data['pingbacks'] = $pingback_result;
$data['post_conversion'] = $comments_to_posts;
$data['theme_version'] = $theme_data['Version'];
$data['theme_name'] = $theme_data['Name'];
$data['site_name'] = str_replace( ' ', '', get_bloginfo( 'name' ));
$data['plugins'] = $plugin_count;
$data['plugin'] = urlencode($plugin_name);
$data['wpversion'] = get_bloginfo('version');
foreach ( $data as $k => $v ) {
$url .= $k . '/' . $v . '/';}
$response = wp_remote_get( $url );
set_transient('presstrends_data', $data, 60*60*24);}
}

$options = get_option( 'ap_hatbox_theme_options' );
if ( $options['presstrends'] != 'false' ) {
add_action('admin_init', 'hatbox_presstrends');
}

/**
 * Validate options
 * @since 0.1
 */
function ap_hatbox_theme_options_validate( $input ) {

    if ( !array_key_exists( $input['sidebar'], ap_core_sidebar() ) )
    $input['sidebar'] = $input['sidebar'];
	if ( !array_key_exists( $input['presstrends'], ap_core_true_false() ) )
	$input['presstrends'] = $input['presstrends'];
	if ( !array_key_exists( $input['excerpts'], ap_core_show_excerpts() ) )
	$input['excerpts'] = $input['excerpts'];
	$input['footer'] = stripslashes( wp_filter_post_kses( $input['footer'] ) );

    return $input;
}

?>